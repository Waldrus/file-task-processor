CREATE DOMAIN dt_component_name AS CHARACTER VARYING(16);
CREATE DOMAIN dt_log_status AS CHARACTER VARYING(8);
CREATE DOMAIN dt_task_state AS CHARACTER VARYING(16);

CREATE DOMAIN dt_processor_code AS CHARACTER(5) NOT NULL
  CONSTRAINT dt_processor_code_check CHECK ((VALUE ~ '^[A-Z0-9]{5}$' :: TEXT));

CREATE DOMAIN dt_property_code AS CHARACTER VARYING(32)
  CONSTRAINT dt_property_code_check CHECK (((VALUE) :: TEXT ~ '^[a-z0-9.-]{3,32}$' :: TEXT));

CREATE DOMAIN dt_property_value AS CHARACTER VARYING(256);
CREATE DOMAIN dt_task_file_name AS CHARACTER VARYING(128);


CREATE TABLE public.task_processors
(
  processor_id   SERIAL           NOT NULL, -- Внутренний идентификатор обработчика
  processor_code dt_processor_code NOT NULL, -- Код обработчика для программ
  processor_name CHARACTER VARYING(256), -- Понятное название обработчика
  PRIMARY KEY (processor_id),
  UNIQUE (processor_code)
);
COMMENT ON TABLE public.task_processors
IS 'Обработчики заданий';
COMMENT ON COLUMN public.task_processors.processor_id IS 'Внутренний идентификатор обработчика';
COMMENT ON COLUMN public.task_processors.processor_code IS 'Код обработчика для программ';
COMMENT ON COLUMN public.task_processors.processor_name IS 'Понятное название обработчика';


CREATE TABLE public.task_processors_settings
(
  processor_id integer NOT NULL,
  property_code dt_property_code NOT NULL, -- Код параметра
  property_value dt_property_value, -- Значение параметра
  PRIMARY KEY (processor_id, property_code),
  FOREIGN KEY (processor_id)
  REFERENCES public.task_processors (processor_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);
COMMENT ON TABLE public.task_processors_settings
IS 'Настройки обработчиков заданий';
COMMENT ON COLUMN public.task_processors_settings.property_code IS 'Код параметра';
COMMENT ON COLUMN public.task_processors_settings.property_value IS 'Значение параметра';

CREATE TABLE public.task_processors_logs
(
  processor_id integer NOT NULL,
  processor_component dt_component_name NOT NULL,
  log_time timestamp without time zone NOT NULL,
  log_status dt_log_status NOT NULL,
  log_message text NOT NULL,
  FOREIGN KEY (processor_id)
  REFERENCES public.task_processors (processor_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);
COMMENT ON TABLE public.task_processors_logs
IS 'Логи обработчиков заданий';


CREATE TABLE public.tasks
(
  processor_id integer NOT NULL,
  task_id SERIAL NOT NULL,
  task_state dt_task_state NOT NULL,
  task_create_time timestamp without time zone NOT NULL,
  task_update_time timestamp without time zone NOT NULL,
  file_name dt_task_file_name NOT NULL,
  task_data text,
  PRIMARY KEY (task_id),
  UNIQUE (processor_id, file_name),
  FOREIGN KEY (processor_id)
  REFERENCES public.task_processors (processor_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);
COMMENT ON TABLE public.tasks
IS 'Задачи';


CREATE TABLE public.task_log
(
  log_time timestamp without time zone NOT NULL,
  log_status dt_log_status NOT NULL,
  file_name dt_task_file_name NOT NULL,
  log_message text NOT NULL
);
COMMENT ON TABLE public.task_log
IS 'Лог обработки задач';