package com.test.filetaskprocessor;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class ExecutorsConfiguration {

    @Value("${app.scan.threads:-1}")
    private int scanThreadCount;

    @Value("${app.scan.queue-size:-1}")
    private int scanTaskQueueSize;

    @Bean
    @Qualifier("scanSourceTaskExecutor")
    public ThreadPoolTaskExecutor scanSourceTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

        if (scanThreadCount > 0) {
            executor.setMaxPoolSize(scanThreadCount);
        }

        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());

        /**
         * Ограничение на размер очереди заданий в памяти
         */
        executor.setQueueCapacity(scanTaskQueueSize);

        executor.setThreadNamePrefix("thread-scan-");
        executor.initialize();

        return executor;
    }

    @Bean
    public TaskScheduler taskScheduler() {
        //3 потока для задач: сканирование, обработка, обновление конфига
        return new ConcurrentTaskScheduler(Executors.newScheduledThreadPool(3));

    }
}
