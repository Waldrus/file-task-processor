package com.test.filetaskprocessor;

import org.springframework.util.Assert;

/**
 * Доступные политики обработки ошибок при старте приложения
 */
public enum ConfigStartupErrorPolicy {
    SILENT, EXIT;

    public static ConfigStartupErrorPolicy valueByName(String name){
        Assert.hasText(name, "Передано некорректное имя политики");
        switch (name.toUpperCase()){
            case "SILENT":
                return SILENT;

            case "EXIT":
                return EXIT;

                default:
                    throw new IllegalArgumentException("Указанная политика не поддерживается: " + name);
        }
    }
}
