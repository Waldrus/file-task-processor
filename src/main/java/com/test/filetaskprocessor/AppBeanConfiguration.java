package com.test.filetaskprocessor;

import com.test.filetaskprocessor.beans.config.AppIdentity;
import com.test.filetaskprocessor.beans.config.LocalConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppBeanConfiguration {

    @Value("${app.code}")
    private String appCode;

    @Bean
    public AppIdentity appIdentity() {
        return new AppIdentity(appCode);
    }

    @Bean
    @ConfigurationProperties(prefix = "app.config.local")
    public LocalConfig localConfig() {
        return new LocalConfig();
    }
}
