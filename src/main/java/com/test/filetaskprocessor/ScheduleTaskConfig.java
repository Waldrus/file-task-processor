package com.test.filetaskprocessor;

import com.test.filetaskprocessor.beans.config.ConfigurationService;
import com.test.filetaskprocessor.beans.config.TaskConfig;
import com.test.filetaskprocessor.beans.config.exceptions.ConfigurationException;
import com.test.filetaskprocessor.beans.process.ProcessFileService;
import com.test.filetaskprocessor.beans.scan.ScanSourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class ScheduleTaskConfig {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleTaskConfig.class);

    @Autowired
    private ScanSourceService scanService;

    @Autowired
    private ProcessFileService processService;

    @Autowired
    private ConfigurationService configurationService;

    @Value("${app.config.startup-error:exit}")
    private String configStartupPolicy;

    private AtomicReference<TaskConfig> config = new AtomicReference<>();

    /**
     * Инициализация конфигурации приложения и очистка необработанных задач при старте приложения
     */
    @PostConstruct
    public void init() {
        try {
            readAndUpdateConfiguration();
            clean();
        } catch (ConfigurationException e) {
            if (ConfigStartupErrorPolicy.EXIT.equals(ConfigStartupErrorPolicy.valueByName(configStartupPolicy))) {
                /**
                 * Выбрасываем исключение наверх, чтобы приложение завершилось с ошибкой
                 * Здесь делается ставка, что неработающее приложение привлечет внимание и администратор вынужден
                 * будет читать логи в поисках ошибки
                 * Если же приложение запустится и будет перечитывать конфиг в ожидании корректных настроек, обнаружение
                 * ошибки может произойти не скоро
                 */
                throw new RuntimeException("Ошибка конфигурации");
            }
        }
    }

    /**
     * Обновления конфигурации по расписанию
     * Конфигурация читается при старте приложения, нет смысла ее еще раз перечитывать по расписанию
     *
     * @throws ConfigurationException
     */
    @Scheduled(fixedRateString = "#{${app.config.update-interval:10} * 1000}", initialDelayString = "#{${app.config.update-interval:10} * 1000}")
    public void update() {
        try {
            readAndUpdateConfiguration();
        } catch (ConfigurationException e) {
            //nothing
        }
    }

    /**
     * Сканирование источника заданий по расписанию
     */
    @Scheduled(fixedRateString = "#{${app.scan.interval:10} * 1000}")
    public void scan() {
        TaskConfig cfg = config.get();
        if (null != cfg) {
            scanService.scan(cfg);
        } else {
            LOG.warn("Сканироение источника задание не выполнено, не инициализирована конфигурация");
        }
    }

    /**
     * Обработка новых заданий по раскичанию
     */
    @Scheduled(fixedRateString = "#{${app.process.interval:10} * 1000}")
    public void process() {
        TaskConfig cfg = config.get();
        if (null != cfg) {
            processService.process(cfg);
        } else {
            LOG.warn("Обработка заданий не выполнена, не инициализирована конфигурация");
        }
    }

    private void readAndUpdateConfiguration() throws ConfigurationException {
        TaskConfig cfg = configurationService.read();
        config.set(cfg);
    }

    /**
     * Зачистка необработанных файлов в результате некорректного завершения приложения
     */
    private void clean() {
        TaskConfig cfg = config.get();
        scanService.clean(cfg.getTempPath());
    }


}
