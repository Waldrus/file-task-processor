package com.test.filetaskprocessor.utils;

public class ErrorUtils {
    public static Throwable getInitialCause(Throwable e){
        while (null != e.getCause()){
            e = e.getCause();
        }
        return e;
    }
}
