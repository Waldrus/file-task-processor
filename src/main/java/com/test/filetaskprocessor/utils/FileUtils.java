package com.test.filetaskprocessor.utils;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtils {
    public static void deleteIfExistsSilent(Path path){
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            //nothing
        }
    }

    public static void createDirectoriesIfNotExists(Path path) throws IOException {
        if (Files.notExists(path)){
            try {
                Files.createDirectories(path);
            } catch (FileAlreadyExistsException e) {
                //nothing
            }
        }
    }


}
