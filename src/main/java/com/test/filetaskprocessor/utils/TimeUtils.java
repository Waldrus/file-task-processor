package com.test.filetaskprocessor.utils;

import java.time.Instant;
import java.util.Date;

public class TimeUtils {
    public static Date nowUTC(){
        return new Date(Instant.now().toEpochMilli());
    }
}
