package com.test.filetaskprocessor.dao.process;

import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Date;

@Repository
public class ProcessorDaoImpl implements ProcessorDao {
    private static final String INSERT_LOG_SQL  = "INSERT INTO task_processors_logs(processor_id, log_time, log_status, processor_component, log_message) VALUES (:id, :time, :status, :component, :message)";
    private static final String READ_APP_ID_SQL  = "SELECT processor_id FROM task_processors WHERE processor_code = :appCode";

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ProcessorDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Integer readProcessorId(String appCode) {
        return jdbcTemplate.queryForObject(READ_APP_ID_SQL, Collections.singletonMap("appCode", appCode), Integer.class);
    }

    @Override
    public void writeLog(Integer processorId, Date time, String component, LogStatus status, String message) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("id", processorId);
        source.addValue("time", time);
        source.addValue("component", component);
        source.addValue("status", status.name());
        source.addValue("message", message);

        jdbcTemplate.update(INSERT_LOG_SQL, source);
    }


}
