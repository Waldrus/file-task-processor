package com.test.filetaskprocessor.dao.process;

import com.test.filetaskprocessor.dao.process.entity.LogStatus;

import java.util.Date;

public interface ProcessorDao {
    Integer readProcessorId(String appCode);
    void writeLog(Integer processorId, Date time, String component, LogStatus status, String message);
}
