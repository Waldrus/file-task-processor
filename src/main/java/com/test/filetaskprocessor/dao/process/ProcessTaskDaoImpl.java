package com.test.filetaskprocessor.dao.process;

import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.dao.process.entity.TaskState;
import com.test.filetaskprocessor.dao.process.entity.TaskStateEntity;
import com.test.filetaskprocessor.dao.process.mappers.TaskStatusMapper;
import com.test.filetaskprocessor.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class ProcessTaskDaoImpl implements ProcessTaskDao {

    private static final String CREATE_TASK_SQL = "INSERT INTO tasks(processor_id, file_name, task_state, task_create_time, task_update_time) VALUES(:processorId, :fileName, :state, :time, :time) RETURNING processor_id";
    private static final String WRITE_TASK_CONTENT_SQL = "UPDATE tasks SET task_state = :state, task_update_time = :time, task_data = :data WHERE task_id = :taskId";
    private static final String UPDATE_TASK_STATUS_SQL = "UPDATE tasks SET task_state = :state, task_update_time = :time WHERE task_id = :taskId";
    private static final String READ_TASK_STATUS_SQL = "SELECT task_id, task_state, task_update_time FROM tasks WHERE processor_id = :processorId AND file_name = :fileName";
    private static final String VERIFY_AND_UPDATE_TASK_STATUS_SQL = "UPDATE tasks SET task_state = :state, task_update_time = :time WHERE task_id = :taskId AND task_update_time = :updateTime";

    private static final String INSERT_TASK_LOG_SQL = "INSERT INTO task_log(log_time, log_status, file_name, log_message) VALUES(:time, :status, :fileName, :message)";

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ProcessTaskDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Integer registerTask(Integer processorId, String fileName) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("processorId", processorId);
        source.addValue("fileName", fileName);
        source.addValue("time", TimeUtils.nowUTC());
        source.addValue("state", TaskState.NEW.name());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(CREATE_TASK_SQL, source, keyHolder);
        return (Integer) keyHolder.getKey();
    }

    @Override
    public TaskStateEntity readTaskStatus(Integer processorId, String fileName) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("processorId", processorId);
        source.addValue("fileName", fileName);

        return jdbcTemplate.queryForObject(READ_TASK_STATUS_SQL, source, new TaskStatusMapper());
    }

    @Override
    public void writeTaskDataAndUpdateState(Integer taskId, String data, TaskState state) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("taskId", taskId);
        source.addValue("data", data);
        source.addValue("state", state.name());
        source.addValue("time", TimeUtils.nowUTC());

        jdbcTemplate.update(WRITE_TASK_CONTENT_SQL, source);
    }

    public boolean verifyAndUpdateTaskStatus(TaskStateEntity state, TaskState newState) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("taskId", state.getTaskId());
        source.addValue("updateTime", state.getUpdateTime());
        source.addValue("time", TimeUtils.nowUTC());
        source.addValue("state", newState.name());

        return jdbcTemplate.update(VERIFY_AND_UPDATE_TASK_STATUS_SQL, source) > 0;
    }

    @Override
    public void writeTaskState(Integer taskId, TaskState state) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("taskId", taskId);
        source.addValue("state", state.name());
        source.addValue("time", TimeUtils.nowUTC());

        jdbcTemplate.update(UPDATE_TASK_STATUS_SQL, source);
    }

    @Override
    public void writeTaskLog(Date time, LogStatus status, String fileName, String message) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("time", time);
        source.addValue("status", status.name());
        source.addValue("fileName", fileName);
        source.addValue("message", message);

        jdbcTemplate.update(INSERT_TASK_LOG_SQL, source);
    }
}
