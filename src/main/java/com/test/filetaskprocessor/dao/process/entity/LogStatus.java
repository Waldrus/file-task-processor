package com.test.filetaskprocessor.dao.process.entity;

/**
 * Статусы сообщений в логе задач
 * INFO - информационное сообщение
 * WARN - предупреждение
 * ERROR - сообщение об ошибке обработки
 * START - собщение о начале выполнения операции
 * SUCCESS - сообщение об успешном завершении операции
 */
public enum LogStatus {
    INFO, WARN, ERROR, START, SUCCESS;
}
