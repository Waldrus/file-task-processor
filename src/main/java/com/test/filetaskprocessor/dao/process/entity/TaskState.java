package com.test.filetaskprocessor.dao.process.entity;

public enum TaskState {
    NEW, DATA_SAVED, JMS_SEND, SUCCESS, ERROR;
}
