package com.test.filetaskprocessor.dao.process.mappers;

import com.test.filetaskprocessor.dao.process.entity.TaskState;
import com.test.filetaskprocessor.dao.process.entity.TaskStateEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskStatusMapper implements RowMapper<TaskStateEntity> {
    private static final String FIELD_TASK_ID = "task_id";
    private static final String FIELD_TASK_STATE = "task_state";
    private static final String FIELD_TASK_UPDATE_TIME = "task_update_time";

    @Override
    public TaskStateEntity mapRow(ResultSet rs, int i) throws SQLException {
        TaskStateEntity e = new TaskStateEntity(rs.getInt(FIELD_TASK_ID), TaskState.valueOf(rs.getString(FIELD_TASK_STATE)), rs.getTimestamp(FIELD_TASK_UPDATE_TIME));

        return e;
    }
}
