package com.test.filetaskprocessor.dao.process;

import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.dao.process.entity.TaskState;
import com.test.filetaskprocessor.dao.process.entity.TaskStateEntity;

import java.util.Date;

public interface ProcessTaskDao {
    Integer registerTask(Integer processorId, String fileName);

    TaskStateEntity readTaskStatus(Integer processorId, String fileName);

    boolean verifyAndUpdateTaskStatus(TaskStateEntity state, TaskState newState);

    void writeTaskDataAndUpdateState(Integer taskId, String data, TaskState state);

    void writeTaskState(Integer taskId, TaskState state);

    void writeTaskLog(Date time, LogStatus status, String fileName, String message);
}
