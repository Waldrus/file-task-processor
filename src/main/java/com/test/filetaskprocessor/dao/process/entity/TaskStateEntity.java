package com.test.filetaskprocessor.dao.process.entity;

import java.util.Date;

public class TaskStateEntity {
    private Integer taskId;
    private TaskState state;
    private Date updateTime;

    public TaskStateEntity() {
    }

    public TaskStateEntity(Integer id, TaskState state, Date updateTime) {
        this.taskId = id;
        this.state = state;
        this.updateTime = updateTime;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TaskStateEntity{" +
                "taskId=" + taskId +
                ", state=" + state +
                ", updateTime=" + updateTime +
                '}';
    }
}
