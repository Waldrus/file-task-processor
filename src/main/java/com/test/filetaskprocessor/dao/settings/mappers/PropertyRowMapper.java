package com.test.filetaskprocessor.dao.settings.mappers;

import com.test.filetaskprocessor.dao.settings.entity.PropertyEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PropertyRowMapper implements RowMapper<PropertyEntity> {
    private static final String FIELD_PROPERTY_CODE = "property_code";
    private static final String FIELD_PROPERTY_VALUE = "property_value";

    @Override
    public PropertyEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        PropertyEntity property = new PropertyEntity(resultSet.getString(FIELD_PROPERTY_CODE));
        String value = resultSet.getString(FIELD_PROPERTY_VALUE);
        if (!resultSet.wasNull()){
            property.setValue(StringUtils.trim(value));
        }
        return property;
    }
}
