package com.test.filetaskprocessor.dao.settings.entity;

import org.springframework.util.Assert;

public class PropertyEntity {

    private String code;

    private String value;

    public PropertyEntity(String code) {
        Assert.hasText(code, "Не указан код свойства");
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "PropertyEntity{" +
                "code='" + code + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
