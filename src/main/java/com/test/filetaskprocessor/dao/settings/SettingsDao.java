package com.test.filetaskprocessor.dao.settings;

import com.test.filetaskprocessor.dao.settings.entity.PropertyEntity;

import java.util.List;

public interface SettingsDao {
    List<PropertyEntity> readSettings(String appCode);
}
