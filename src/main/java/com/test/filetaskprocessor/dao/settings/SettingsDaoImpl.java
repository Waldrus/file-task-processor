package com.test.filetaskprocessor.dao.settings;

import com.test.filetaskprocessor.dao.settings.entity.PropertyEntity;
import com.test.filetaskprocessor.dao.settings.mappers.PropertyRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

@Repository
public class SettingsDaoImpl implements SettingsDao {

    private static final String READ_PROPERTY_SQL = "SELECT property_code, property_value FROM task_processors_settings " +
            "JOIN task_processors USING (processor_id) WHERE processor_code = :appCode";

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public SettingsDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public List<PropertyEntity> readSettings(String appCode) {
        Assert.hasText(appCode, "Не передан код приложения");
        List<PropertyEntity> properties = jdbcTemplate.query(READ_PROPERTY_SQL,
                Collections.singletonMap("appCode", appCode), new PropertyRowMapper());
        return properties;
    }


}
