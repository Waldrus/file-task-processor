package com.test.filetaskprocessor.beans;

import com.test.filetaskprocessor.beans.config.AppIdentity;
import com.test.filetaskprocessor.dao.process.ProcessTaskDao;
import com.test.filetaskprocessor.dao.process.ProcessorDao;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.utils.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

abstract public class BaseProcessorService {
    private static final Logger LOG = LoggerFactory.getLogger(BaseProcessorService.class);

    private final static Map<String, Integer> codeCache = new ConcurrentHashMap<>();
    private final String componentCode;
    private final String appCode;

    private final ProcessorDao processorDao;
    private final ProcessTaskDao processTaskDao;

    public BaseProcessorService(AppIdentity appIdentity, String componentCode, ProcessorDao processorDao, ProcessTaskDao processTaskDao) {
        Assert.notNull(appIdentity, "Не задан идентификатор приложения");
        Assert.hasText(componentCode, "Не задан код компонента");
        Assert.notNull(processorDao, "Не задан ProcessorDao");

        this.appCode = appIdentity.getAppCode();
        this.componentCode = componentCode;
        this.processorDao = processorDao;
        this.processTaskDao = processTaskDao;
    }

    public Integer getProcessorId(){
        return getProcessorId(appCode);
    }

    private Integer getProcessorId(String appCode) {
        if (!codeCache.containsKey(appCode)) {
            /**
             * Поскольку мы заведомо знаем, что appCode короткая строка и не подразумевается большое количество вариаций
             * в одном приложении, используем appCode.intern() для синхронизации загрузки идентификатора с одинаковыми кодами
             */
            synchronized (appCode.intern()) {
                if (!codeCache.containsKey(appCode)) {
                    Integer id = processorDao.readProcessorId(appCode);
                    codeCache.put(appCode, id);
                }
            }
        }
        return codeCache.get(appCode);
    }

    public void writeLog(LogStatus status, String message){
        Integer id = getProcessorId();
        LOG.debug("Запись лога в БД: {} {}", status, message);
        processorDao.writeLog(id, TimeUtils.nowUTC(), componentCode, status, message);
    }

    public void writeTaskLog(String fileName, LogStatus state, String message) {
        processTaskDao.writeTaskLog(TimeUtils.nowUTC(), state, fileName, message);
    }

}
