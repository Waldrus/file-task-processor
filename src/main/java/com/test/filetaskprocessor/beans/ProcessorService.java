package com.test.filetaskprocessor.beans;

import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;

public interface ProcessorService {
    Integer getProcessorId();

    void writeLog(LogStatus status, String message);

    void writeTaskLog(String fileName, LogStatus state, String message);
}
