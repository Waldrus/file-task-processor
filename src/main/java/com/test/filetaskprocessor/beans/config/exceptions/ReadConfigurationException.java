package com.test.filetaskprocessor.beans.config.exceptions;

public class ReadConfigurationException extends ConfigurationException {
    public ReadConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
