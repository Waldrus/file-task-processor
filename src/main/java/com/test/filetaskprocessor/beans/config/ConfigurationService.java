package com.test.filetaskprocessor.beans.config;

import com.test.filetaskprocessor.beans.config.exceptions.ConfigurationException;

public interface ConfigurationService {
    TaskConfig read() throws ConfigurationException;
}
