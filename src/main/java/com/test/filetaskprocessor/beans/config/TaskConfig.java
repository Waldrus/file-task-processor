package com.test.filetaskprocessor.beans.config;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Конфиг для обработчиков задач READ ONLY
 */
public class TaskConfig {
    private final Path sourcePath;
    private final String fileMask;
    private final Path archivePath;
    private final Path tempPath;
    private final int retryTaskDelay;

    public TaskConfig(LocalConfig source) {
        sourcePath = Paths.get(source.getSourcePath());
        fileMask = source.getFileMask();
        archivePath = Paths.get(source.getWorkPath(), source.getArchiveFolder());
        tempPath = Paths.get(source.getWorkPath(), source.getTempFolder());
        retryTaskDelay = source.getRetryTaskDelay();
    }

    public Path getSourcePath() {
        return sourcePath;
    }

    public String getFileMask() {
        return fileMask;
    }

    public Path getArchivePath() {
        return archivePath;
    }

    public Path getTempPath() {
        return tempPath;
    }

    public int getRetryTaskDelay() {
        return retryTaskDelay;
    }

    @Override
    public String toString() {
        return "TaskConfig{" +
                "sourcePath=" + sourcePath +
                ", fileMask='" + fileMask + '\'' +
                ", archivePath=" + archivePath +
                ", tempPath=" + tempPath +
                ", retryTaskDelay=" + retryTaskDelay +
                '}';
    }
}
