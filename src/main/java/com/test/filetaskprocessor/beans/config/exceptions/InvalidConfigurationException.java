package com.test.filetaskprocessor.beans.config.exceptions;

public class InvalidConfigurationException extends ConfigurationException {
    public InvalidConfigurationException(String message) {
        super(message);
    }
}
