package com.test.filetaskprocessor.beans.config;

import com.test.filetaskprocessor.beans.BaseProcessorService;
import com.test.filetaskprocessor.dao.process.ProcessTaskDao;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.beans.config.exceptions.ConfigurationException;
import com.test.filetaskprocessor.beans.config.exceptions.InvalidConfigurationException;
import com.test.filetaskprocessor.beans.config.exceptions.ReadConfigurationException;
import com.test.filetaskprocessor.dao.process.ProcessorDao;
import com.test.filetaskprocessor.dao.settings.SettingsDao;
import com.test.filetaskprocessor.dao.settings.entity.PropertyEntity;
import com.test.filetaskprocessor.utils.ErrorUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConfigurationServiceImpl extends BaseProcessorService implements ConfigurationService {
    private static final String COMPONENT_CODE = "CONFIGURATION";

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationServiceImpl.class);

    private static final String CONFIG_SOURCE_PROPERTY_ID = "source";
    private final SettingsDao settingsRepository;
    private final LocalConfig localConfig;
    private final String appCode;

    @Autowired
    public ConfigurationServiceImpl(AppIdentity appIdentity, LocalConfig localConfig, ProcessorDao processorDao, ProcessTaskDao processTaskDao, SettingsDao settingsRepository) {
        super(appIdentity, COMPONENT_CODE, processorDao, processTaskDao);

        Assert.notNull(localConfig, "Не указаны базовые параметры приложения");
        Assert.notNull(settingsRepository, "Не указан репозиторий параметров приложения");

        this.settingsRepository = settingsRepository;
        this.localConfig = localConfig;
        this.appCode = appIdentity.getAppCode();
    }

    @Override
    public TaskConfig read() throws ConfigurationException {
        try {
            return readInternal();
        } catch (ConfigurationException e) {
            LOG.error(e.getMessage(), e);
            writeLog(LogStatus.ERROR, e.getMessage());
            throw e;
        } catch (RuntimeException e) {
            LOG.error("Непредвиденная ошибка чтения конфигурации приложения", e);
            writeLog(LogStatus.ERROR, "Непредвиденная ошибка чтения конфигурации приложения: " + ErrorUtils.getInitialCause(e));
            throw e;
        }
    }

    private TaskConfig readInternal() throws ConfigurationException {
        Map<String, String> properties = readExternalProperties();
        LOG.debug("Загруженные параметры приложения: {}", properties);

        LocalConfig config = new LocalConfig(localConfig);
        applyExternalProperties(config, properties);

        LOG.debug("Итоговые параметры приложения: {}", config);

        validate(config);
        return new TaskConfig(config);
    }

    private Map<String, String> readExternalProperties() throws ReadConfigurationException {
        try {
            List<PropertyEntity> properties = settingsRepository.readSettings(appCode);
            return convert(properties);
        } catch (DataAccessException e) {
            LOG.warn("Ошибка загрузки настроек приложений", e);
            throw new ReadConfigurationException("Не удалось загрузить настройки", e);
        }
    }

    private Map<String, String> convert(List<PropertyEntity> properties) {
        Map<String, String> result = new HashMap<>();
        for (PropertyEntity property : properties) {
            result.put(property.getCode(), property.getValue());
        }
        return result;
    }

    private void applyExternalProperties(LocalConfig config, Map<String, String> properties) {
        for (Map.Entry<String, String> property : properties.entrySet()) {
            switch (property.getKey()) {
                case CONFIG_SOURCE_PROPERTY_ID:
                    config.setSourcePath(property.getValue());
                    break;
            }
        }
    }

    protected void validate(LocalConfig config) throws InvalidConfigurationException {
        if (StringUtils.isBlank(config.getSourcePath())) {
            throw new InvalidConfigurationException("Не задан путь к источнику заданий");
        }

        if (StringUtils.isBlank(config.getWorkPath())) {
            throw new InvalidConfigurationException("Не задан путь к рабочей директории приложения");
        }

        if (StringUtils.isBlank(config.getFileMask())) {
            throw new InvalidConfigurationException("Не задан маска для обрабатываемых файлов");
        }

        if (StringUtils.isBlank(config.getArchiveFolder())) {
            throw new InvalidConfigurationException("Не задана папка для архива файлов");
        }

        if (StringUtils.isBlank(config.getTempFolder())) {
            throw new InvalidConfigurationException("Не задана папка для временных файлов");
        }
    }
}
