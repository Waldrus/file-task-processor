package com.test.filetaskprocessor.beans.config;

public class LocalConfig {
    private String sourcePath;
    private String workPath;
    private String fileMask = "*";
    private String archiveFolder = "archive";
    private String tempFolder = "temp";
    private int retryTaskDelay = 5 * 60 * 1000;//5 min

    public LocalConfig() {
    }

    public LocalConfig(LocalConfig source) {
        sourcePath = source.sourcePath;
        workPath = source.workPath;
        fileMask = source.fileMask;
        archiveFolder = source.archiveFolder;
        tempFolder = source.tempFolder;
        retryTaskDelay = source.retryTaskDelay;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getWorkPath() {
        return workPath;
    }

    public void setWorkPath(String workPath) {
        this.workPath = workPath;
    }

    public String getFileMask() {
        return fileMask;
    }

    public void setFileMask(String fileMask) {
        this.fileMask = fileMask;
    }

    public String getArchiveFolder() {
        return archiveFolder;
    }

    public void setArchiveFolder(String archiveFolder) {
        this.archiveFolder = archiveFolder;
    }

    public String getTempFolder() {
        return tempFolder;
    }

    public void setTempFolder(String tempFolder) {
        this.tempFolder = tempFolder;
    }

    public int getRetryTaskDelay() {
        return retryTaskDelay;
    }

    public void setRetryTaskDelay(int retryTaskDelay) {
        this.retryTaskDelay = retryTaskDelay;
    }

    @Override
    public String toString() {
        return "LocalConfig{" +
                "sourcePath='" + sourcePath + '\'' +
                ", workPath='" + workPath + '\'' +
                ", fileMask='" + fileMask + '\'' +
                ", archiveFolder='" + archiveFolder + '\'' +
                ", tempFolder='" + tempFolder + '\'' +
                ", retryTaskDelay=" + retryTaskDelay +
                '}';
    }
}
