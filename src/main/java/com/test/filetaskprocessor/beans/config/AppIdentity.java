package com.test.filetaskprocessor.beans.config;

import org.springframework.util.Assert;

public class AppIdentity {
    private final String appCode;

    public AppIdentity(String appCode) {
        Assert.hasText(appCode, "Не указан код приложения");
        this.appCode = appCode;
    }

    public String getAppCode() {
        return appCode;
    }

    @Override
    public String toString() {
        return "AppIdentity{" +
                "appCode='" + appCode + '\'' +
                '}';
    }
}
