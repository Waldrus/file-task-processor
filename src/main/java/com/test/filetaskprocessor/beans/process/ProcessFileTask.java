package com.test.filetaskprocessor.beans.process;

import com.test.filetaskprocessor.beans.ProcessorService;
import com.test.filetaskprocessor.beans.config.TaskConfig;
import com.test.filetaskprocessor.beans.process.exceptions.FatalProcessException;
import com.test.filetaskprocessor.beans.process.exceptions.ProcessException;
import com.test.filetaskprocessor.dao.process.ProcessTaskDao;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.dao.process.entity.TaskState;
import com.test.filetaskprocessor.dao.process.entity.TaskStateEntity;
import com.test.filetaskprocessor.utils.ErrorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;

public class ProcessFileTask {
    private static final Logger LOG = LoggerFactory.getLogger(ProcessFileTask.class);

    private final ProcessorService service;
    private final Path file;
    private final Path archiveFolder;
    private final ProcessTaskDao processTaskDao;
    private final int restoreTaskDelay;//ms

    private Integer taskId;

    public ProcessFileTask(ProcessorService service, Path file, TaskConfig config, ProcessTaskDao processTaskDao) {
        this.service = service;
        this.file = file;
        this.archiveFolder = config.getArchivePath();
        this.restoreTaskDelay = config.getRetryTaskDelay();
        this.processTaskDao = processTaskDao;
    }

    public void process() {
        try {
            processInternal();
        } catch (FatalProcessException e){
            LOG.error(e.getMessage(), e);
            //Фатальные ошибки переводят задачу в статус ERROR и больше не повторяется выполнение
            writeTaskLog(LogStatus.ERROR, e.getMessage());
            setTaskState(TaskState.ERROR);

            try {
                moveToArchiveFolder();
            } catch (ProcessException ex) {
                LOG.error(ex.getMessage(), ex);
            }
        } catch (ProcessException e) {
            LOG.error(e.getMessage(), e);
            //Различные ошибки доступа к внешним системам, откладываем обработку
            writeTaskLog(LogStatus.WARN, e.getMessage());
        }
    }

    private void processInternal() throws ProcessException {
        String fileName = getFileName();
        try {
            taskId = processTaskDao.registerTask(service.getProcessorId(), fileName);
            writeTaskLog(LogStatus.START, "Начало обработки задания: " + taskId);
            processTaskStep1();
        } catch (DuplicateKeyException e) {
            continueAbandondTask();
        }
    }

    private void processTaskStep1() throws ProcessException {
        String content = readContent();

        writeContentToStorage(content);
        setTaskState(TaskState.DATA_SAVED);
        writeTaskLog(LogStatus.INFO, "Данные сохранены в БД");

        processTaskStep2(content);
    }

    private void processTaskStep2(String content) throws ProcessException {
        String fileName = getFileName();

        sendMessage(fileName, content);
        setTaskState(TaskState.JMS_SEND);
        writeTaskLog(LogStatus.INFO, "Сообщение отправлено");

        processTaskStep3();
    }

    private void processTaskStep3() throws ProcessException {
        Path archiveFile = moveToArchiveFolder();
        setTaskState(TaskState.SUCCESS);
        writeTaskLog(LogStatus.SUCCESS, "Обработка выполнена, файл перемещен в архив: " + archiveFile);
    }

    /**
     * Метод продолжает выполнение задачи, которая была прекрашена по неизвестным причинам
     * @throws ProcessException
     */
    private void continueAbandondTask() throws ProcessException {
        String fileName = getFileName();
        TaskStateEntity state = processTaskDao.readTaskStatus(service.getProcessorId(), fileName);

        /**
         * Проверка времени последнего изменения задачи
         * и ожидание restoreTaskDelay до повторной обработки, вдруг запущено несколько паралельных обработчиков
         */
        if (state.getUpdateTime().before(new Date(Instant.now().toEpochMilli() - restoreTaskDelay))) {
            if (updateTaskStatus(state)) {
                taskId = state.getTaskId();

                writeTaskLog(LogStatus.INFO, "Найдено не завершенно задание, состояние: " + state.getState());

                switch (state.getState()) {
                    case NEW:
                        processTaskStep1();
                        break;

                    case DATA_SAVED:
                        String content = readContent();
                        processTaskStep2(content);
                        break;

                    case JMS_SEND:
                        processTaskStep3();
                        break;

                    case SUCCESS:
                        Path archiveFile = moveToArchiveFolder();
                        writeTaskLog(LogStatus.WARN, "Задание было обработано ранее, перемещаем в архив: " + archiveFile);
                        break;
                }
            } else {
                //Кто-то уже обработал задачу, ничего не делаем
                LOG.info("Не удалось захватить задачу: file={}, task={}", file, state);
            }
        } else {
            LOG.debug("Ожидание таймаута для обработки задачи : file={}, task={}", file, state);
        }
    }

    private String readContent() throws ProcessException {
        try {
            String content = new String(Files.readAllBytes(file));
            LOG.debug("Контент файла: file={} content={}", file, content);
            return content;
        } catch (IOException e) {
            throw new FatalProcessException("Не удалось прочитать файл: " + e.getMessage(), e);
        }
    }

    /**
     * Запись данных задачи в БД
     * метод в 1 транзации записывает данные и изменяет статус задачи
     * @param content
     * @throws ProcessException
     */
    private void writeContentToStorage(String content) throws ProcessException {
        LOG.debug("Запись контента файла в БД: file={}", file);
        try {
            processTaskDao.writeTaskDataAndUpdateState(taskId, content, TaskState.DATA_SAVED);
        } catch (DataAccessException e) {
            throw new ProcessException("Не удалось записать данные в БД: " + ErrorUtils.getInitialCause(e).getMessage(), e);
        }

    }

    /**
     * Отправка сообщения в JMS
     */
    private void sendMessage(String name, String content)  throws ProcessException {
        LOG.debug("Отправка сообщения: file={}", file);
        try {
            //Опыта работы с JMS нет, поэтому ограничимся заглушкой
        } catch (Throwable e) {
            throw new ProcessException("Не удалось отправить сообщение: " + ErrorUtils.getInitialCause(e).getMessage(), e);
        }
    }

    private Path moveToArchiveFolder() throws ProcessException {
        Path archiveFile = Paths.get(archiveFolder.toString(),getFileName());
        try {
            Files.move(file, archiveFile);
            return archiveFile;
        } catch (FileAlreadyExistsException e){
            return moveToArchiveFolderWithDuplicate(1);
        } catch (IOException e) {
            throw new ProcessException(String.format("Не удалось переместить файл в архивную директорию: %s -> %s error=%s", file, archiveFile, e.getMessage()), e);
        }
    }

    private Path moveToArchiveFolderWithDuplicate(int number) throws ProcessException {
        Path dupName = Paths.get(archiveFolder.toString(), getFileName() + "." + number);
        try {
            Files.move(file, dupName);
            return dupName;
        } catch (FileAlreadyExistsException e){
            return moveToArchiveFolderWithDuplicate(number + 1);
        } catch (IOException e) {
            throw new ProcessException(String.format("Не удалось переместить файл в архивную директорию: %s -> %s error=%s", file, dupName, e.getMessage()), e);
        }
    }

    private String getFileName(){
        return file.getFileName().toString();
    }

    protected void setTaskState(TaskState state){
        if (null != taskId) {
            processTaskDao.writeTaskState(taskId, state);
        }
    }

    /**
     * Обновление времени изменения статуса и проверка не был ли статус изменен, чтобы исключить параллельную обработку
     *
     * @param state
     * @return
     */
    private boolean updateTaskStatus(TaskStateEntity state) {
        return processTaskDao.verifyAndUpdateTaskStatus(state, state.getState());
    }

    private void writeTaskLog(LogStatus state, String message) {
        service.writeTaskLog(getFileName(), state, message);
    }

}
