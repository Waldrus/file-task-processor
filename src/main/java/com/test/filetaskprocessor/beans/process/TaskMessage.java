package com.test.filetaskprocessor.beans.process;

public class TaskMessage {
    private String name;
    private String body;

    public TaskMessage(String name, String body) {
        this.name = name;
        this.body = body;
    }

    public TaskMessage() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TaskMessage{");
        sb.append("name='").append(name).append('\'');
        sb.append(", body='").append(body).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
