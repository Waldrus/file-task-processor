package com.test.filetaskprocessor.beans.process;

import com.test.filetaskprocessor.beans.ProcessorService;
import com.test.filetaskprocessor.beans.config.TaskConfig;

public interface ProcessFileService extends ProcessorService {
    /**
     * Запуск поиска новых заданий, их обработка и сохранение в архивтой директории
     *
     * @param config
     */
    void process(TaskConfig config);
}
