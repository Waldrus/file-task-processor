package com.test.filetaskprocessor.beans.process.exceptions;

public class FatalProcessException extends ProcessException {
    public FatalProcessException(String message) {
        super(message);
    }

    public FatalProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}
