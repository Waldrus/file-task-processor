package com.test.filetaskprocessor.beans.process;

import com.test.filetaskprocessor.beans.BaseProcessorService;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.beans.config.AppIdentity;
import com.test.filetaskprocessor.beans.config.TaskConfig;
import com.test.filetaskprocessor.beans.process.exceptions.ProcessException;
import com.test.filetaskprocessor.dao.process.ProcessTaskDao;
import com.test.filetaskprocessor.dao.process.ProcessorDao;
import com.test.filetaskprocessor.utils.ErrorUtils;
import com.test.filetaskprocessor.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.*;

@Service
public class ProcessFileServiceImpl extends BaseProcessorService implements ProcessFileService {
    private static final String COMPONENT_CODE = "PROCESSOR";

    private static final Logger LOG = LoggerFactory.getLogger(ProcessFileServiceImpl.class);

    private final ProcessTaskDao processTaskDao;

    @Autowired
    public ProcessFileServiceImpl(AppIdentity appIdentity, ProcessorDao processorDao, ProcessTaskDao processTaskDao) {
        super(appIdentity, COMPONENT_CODE, processorDao, processTaskDao);

        this.processTaskDao = processTaskDao;
    }

    @Override
    public void process(TaskConfig config) {
        try {
            processInternal(config);
        } catch (RuntimeException e) {
            LOG.error("При обработке заданий произошла непредвиденная ошибка", e);
            writeLog(LogStatus.ERROR, String.format("При обработке заданий произошла непредвиденная ошибка: path=%s, error=%s", config.getTempPath(), ErrorUtils.getInitialCause(e)));
        } catch (ProcessException e){
            LOG.error(e.getMessage(), e);
            writeLog(LogStatus.ERROR, e.getMessage());
        }
    }

    private void processInternal(TaskConfig config) throws ProcessException {
        Assert.notNull(config, "Не указаны параметры");

        Path workFolder = config.getTempPath();
        Path archiveFolder = config.getArchivePath();
        String mask = config.getFileMask();

        LOG.info("Запуск поиска и обработки заданий: path={}", workFolder);
        writeLog(LogStatus.START, String.format("Запуск поиска и обработки заданий: path=%s", workFolder));

        checkTempFolder(workFolder);
        checkArchiveFolder(archiveFolder);

        int fileCount = 0;
        try (DirectoryStream<Path> pathStream = Files.newDirectoryStream(workFolder, mask)) {

            for (Path file : pathStream) {
                LOG.debug("Обработка файла: path={}", file);
                process(file, config);
                fileCount++;
            }

            LOG.info("Обработка заданий выполнена успешно: processed={} path={}", fileCount, workFolder);
            writeLog(LogStatus.SUCCESS, String.format("Обработка заданий выполнена успешно: processed=%s, path=%s", fileCount, workFolder));
        } catch (IOException e) {
            throw new ProcessException(String.format("При сканировании файлов заданий произошла ошибка: processed=%d, path=%s, error=%s", fileCount, workFolder, e));
        }
    }

    private void checkTempFolder(Path tempFolder) throws ProcessException {
        if (Files.notExists(tempFolder)) {
            throw new ProcessException(String.format("Не удалось выполнить сканирование источника, директория не найдена: path=%s", tempFolder));
        }

        if (!Files.isDirectory(tempFolder)) {
            throw new ProcessException(String.format("Не удалось выполнить сканирование источника, источник не является директорией: path=%s", tempFolder));
        }

        if (!Files.isWritable(tempFolder)) {
            throw new ProcessException(String.format("Не удалось выполнить сканирование источника, директория источник не доступна для записи: path=%s", tempFolder));
        }
    }

    private void checkArchiveFolder(Path archiveFolder) throws ProcessException {
        try {
            FileUtils.createDirectoriesIfNotExists(archiveFolder);
        } catch (IOException e) {
            throw new ProcessException(String.format("Не удалось выполнить сканирование источника, не удалось создать рабочую директорию: path=%s error=%s", archiveFolder, e), e);
        }

        if (!Files.isDirectory(archiveFolder)) {
            throw new ProcessException(String.format("Не удалось выполнить сканирование источника, рабочая директория не является директорией: path=%s", archiveFolder));
        }

        if (!Files.isWritable(archiveFolder)) {
            throw new ProcessException(String.format("Не удалось выполнить сканирование источника, рабочая директория источник не доступна для записи: path=%s", archiveFolder));
        }
    }

    private void process(Path file, TaskConfig config) {
        ProcessFileTask task = new ProcessFileTask(this, file, config, processTaskDao);
        task.process();
    }
}
