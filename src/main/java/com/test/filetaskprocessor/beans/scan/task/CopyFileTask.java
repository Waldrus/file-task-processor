package com.test.filetaskprocessor.beans.scan.task;

import com.test.filetaskprocessor.beans.ProcessorService;
import com.test.filetaskprocessor.beans.scan.exceptions.CopyTaskException;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.*;

public class CopyFileTask implements Runnable {
    public static final String TMP_EXT = "tmp";
    private final static Logger LOG = LoggerFactory.getLogger(CopyFileTask.class);
    private final Path sourceFile;
    private final Path destinationFile;
    private final ProcessorService service;
    private final boolean cleanAfterCopy = false;

    public CopyFileTask(ProcessorService service, Path sourceFile, Path destinationFile) {
        Assert.notNull(service, "Не указан сервис");
        Assert.notNull(sourceFile, "Не указан файл для копирования");
        Assert.notNull(destinationFile, "Не указан файл назначения");

        this.service = service;
        this.sourceFile = sourceFile;
        this.destinationFile = destinationFile;
    }

    @Override
    public void run() {
        try {
            runInternal();
        } catch (CopyTaskException e){
            LOG.error(e.getMessage(), e);
            service.writeTaskLog(getFileName(), LogStatus.WARN, e.getMessage());
        } catch (Throwable e){
            LOG.error(e.getMessage(), e);
        }
    }

    private void runInternal() throws CopyTaskException {
        Path tmpPath = createTempFilePath(destinationFile);
        String fileName = sourceFile.getFileName().toString();

        try {
            copySourceToTmpFile(sourceFile, tmpPath);
        } catch (IOException e) {
            throw new CopyTaskException(String.format("Не удалось скопировать файл: %s -> %s error=%s", tmpPath, destinationFile, e));
        }

        try {
            Files.move(tmpPath, destinationFile);

            cleanSourceFile();
            service.writeTaskLog(fileName, LogStatus.SUCCESS, "Файл успешно скопрован из директории источника");
        } catch (IOException e) {
            FileUtils.deleteIfExistsSilent(tmpPath);
            throw new CopyTaskException(String.format("Не удалось переименовать временный файл: %s -> %s error=%s", tmpPath, destinationFile, e));
        }
    }

    private Path createTempFilePath(Path source) {
        /**
         * Добавляем к имени файла хеш от потока, чтобы в случае обработки одного задания разными потоками или приложениями
         * снизить вероятность конфликта(удаления конкурирующим потоком временного файла в методе copySourceToTmpFile) на этапе копирования файла
         */
        return Paths.get(String.format("%s.%s.%s", source, Integer.toHexString(Thread.currentThread().hashCode()), TMP_EXT));
    }

    private void copySourceToTmpFile(Path source, Path tmpFile) throws IOException {
        try {
            Files.copy(source, tmpFile, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (FileAlreadyExistsException e) {
            //Вероятность такого исхода невелика, но поскольку нет механизма определить обрабатывается файл или обрабатывающий процесс упал, удаляем файл и повторяем попытку
            FileUtils.deleteIfExistsSilent(tmpFile);

            Files.copy(source, tmpFile, StandardCopyOption.COPY_ATTRIBUTES);
        }
    }

    private void cleanSourceFile() {
        if (cleanAfterCopy) {
            try {
                Files.delete(sourceFile);
            } catch (IOException e) {
                LOG.error("Не удалось удалить файл из источника: {}", sourceFile);
            }
        }
    }

    private String getFileName(){
        return sourceFile.getFileName().toString();
    }
}
