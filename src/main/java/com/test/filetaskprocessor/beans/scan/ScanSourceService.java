package com.test.filetaskprocessor.beans.scan;

import com.test.filetaskprocessor.beans.ProcessorService;
import com.test.filetaskprocessor.beans.config.TaskConfig;

import java.nio.file.Path;

public interface ScanSourceService extends ProcessorService {
    /**
     * Запуск поиска новых заданий для обработки
     * @param config
     */
    void scan(TaskConfig config);

    /**
     * Удаление файлов, копирование которых не было завершено
     * @param destinationFolder
     */
    void clean(Path destinationFolder);
}
