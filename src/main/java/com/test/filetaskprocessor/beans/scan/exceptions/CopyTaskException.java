package com.test.filetaskprocessor.beans.scan.exceptions;

public class CopyTaskException extends Exception {
    public CopyTaskException(String message) {
        super(message);
    }

    public CopyTaskException(String message, Throwable cause) {
        super(message, cause);
    }
}
