package com.test.filetaskprocessor.beans.scan;

import com.test.filetaskprocessor.beans.BaseProcessorService;
import com.test.filetaskprocessor.beans.config.TaskConfig;
import com.test.filetaskprocessor.dao.process.ProcessTaskDao;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.beans.config.AppIdentity;
import com.test.filetaskprocessor.beans.scan.exceptions.ScanException;
import com.test.filetaskprocessor.beans.scan.task.CopyFileTask;
import com.test.filetaskprocessor.dao.process.ProcessorDao;
import com.test.filetaskprocessor.dao.process.entity.LogStatus;
import com.test.filetaskprocessor.utils.ErrorUtils;
import com.test.filetaskprocessor.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.RejectedExecutionException;

@Service
public class ScanSourceServiceImpl extends BaseProcessorService implements ScanSourceService {
    private static final String COMPONENT_CODE = "SCANNER";

    private static final Logger LOG = LoggerFactory.getLogger(ScanSourceServiceImpl.class);

    private static final int SLEEP_DELAY = 20;//мс

    private ThreadPoolTaskExecutor executorService;

    @Autowired
    public ScanSourceServiceImpl(@Qualifier("scanSourceTaskExecutor") ThreadPoolTaskExecutor executorService, AppIdentity appIdentity, ProcessorDao processorDao, ProcessTaskDao processTaskDao) {
        super(appIdentity, COMPONENT_CODE, processorDao, processTaskDao);

        Assert.notNull(executorService, "Не указан пул потоков для выполнения задач");
        this.executorService = executorService;
    }

    @Override
    public void scan(TaskConfig config) {
        try {
            scanInternal(config);
        } catch (RuntimeException e) {
            LOG.error("При сканирование источника произошла непредвиденная ошибка", e);
            writeLog(LogStatus.ERROR, String.format("При сканирование источника произошла непредвиденная ошибка: path=%s, error={}", config.getSourcePath(), ErrorUtils.getInitialCause(e)));
        } catch (ScanException e) {
            LOG.error(e.getMessage(), e);
            writeLog(LogStatus.ERROR, e.getMessage());
        }
    }

    public void clean(Path tempFolder) {
        Assert.notNull(tempFolder, "Не указана рабочая директория");

        if (Files.notExists(tempFolder)) {
            LOG.info("Рабочая директория не существует, очистка необработанных задач не требуется");
            return;
        }

        try (DirectoryStream<Path> pathStream = Files.newDirectoryStream(tempFolder,
                "*." + CopyFileTask.TMP_EXT)) {
            int fileCount = 0;
            for (Path file : pathStream) {
                cleanFile(file);
                fileCount++;
            }
            LOG.info("Очистка рабочей директории выполнена успешно, файлов найдено {}", fileCount);
        } catch (IOException e) {
            LOG.warn("Не удалось выполнить очистку рабочей директории", e);
        }
    }

    protected void scanInternal(TaskConfig config) throws ScanException {
        Assert.notNull(config, "Не указаны параметры");

        Path sourceFolder = config.getSourcePath();
        Path tempFolder = config.getTempPath();
        String mask = config.getFileMask();

        //Т.к. метод не ждет завершения всех задач, проверяем нет ли в пуле выполняющихся задача, чтобы не создать дубли
        if (executorService.getActiveCount() > 0) {
            LOG.info("Ожидание завершения предыдущих заданий");
            writeLog(LogStatus.INFO, "Ожидание завершения предыдущих заданий");
            return;
        }

        LOG.info("Запуск сканирование источника: mask={}, path={}", mask, sourceFolder);
        writeLog(LogStatus.START, String.format("Запуск сканирование источника: mask=%s, path=%s", mask, sourceFolder));

        checkSourceFolder(sourceFolder);
        checkTempFolder(tempFolder);

        int fileCount = 0;
        try (DirectoryStream<Path> pathStream = Files.newDirectoryStream(sourceFolder, mask)) {

            for (Path file : pathStream) {
                LOG.debug("Создание задание на копирование файла: file={}", file);

                execute(createCopyTask(file, tempFolder));

                fileCount++;
            }
            LOG.info("Сканирование источника выполнено: processed={} path={}", fileCount, sourceFolder);
            writeLog(LogStatus.SUCCESS, String.format("Сканирование источника выполнено: processed=%s, path=%s", fileCount, sourceFolder));
        } catch (IOException e) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, ошибка чтения:  processed=%s, error=%s", fileCount, e), e);
        }
    }

    protected Runnable createCopyTask(Path sourceFile, Path destinationFolder) {
        writeTaskLog(sourceFile.getFileName().toString(), LogStatus.START, "Создано задание на копирование файла");
        return new CopyFileTask(this, sourceFile,
                Paths.get(destinationFolder.toString(), sourceFile.getFileName().toString()));
    }

    private void execute(Runnable task) {
        boolean submitted = false;
        do {
            try {
                executorService.submitListenable(task);
                submitted = true;
            } catch (RejectedExecutionException ex) {
                //Если очередь заданий переполнена ожидаем SLEEP_DELAY милисекунд, затем повторяем попытку
                try {
                    Thread.sleep(SLEEP_DELAY);
                } catch (InterruptedException ie) {
                    throw new RuntimeException(ie);
                }
            }
        } while (!submitted);
    }

    private void cleanFile(Path file) {
        try {
            Files.deleteIfExists(file);
            LOG.info("Не обработанное задание удалено успешно: file={}", file);
        } catch (IOException e) {
            LOG.warn("Не удалось удалить обработанное задание: file={}", file, e);
        }
    }

    private void checkSourceFolder(Path sourceFolder) throws ScanException {
        if (Files.notExists(sourceFolder)) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, директория не найдена: path=%s", sourceFolder));
        }

        if (!Files.isDirectory(sourceFolder)) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, источник не является директорией: path=%s", sourceFolder));
        }

        if (!Files.isWritable(sourceFolder)) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, директория источник не доступна для записи: path=%s", sourceFolder));
        }
    }

    private void checkTempFolder(Path tempFolder) throws ScanException {
        try {
            FileUtils.createDirectoriesIfNotExists(tempFolder);
        } catch (IOException e) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, не удалось создать рабочую директорию: path=%s error=%s", tempFolder, e), e);
        }

        if (!Files.isDirectory(tempFolder)) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, рабочая директория не является директорией: path=%s", tempFolder));
        }

        if (!Files.isWritable(tempFolder)) {
            throw new ScanException(String.format("Не удалось выполнить сканирование источника, рабочая директория источник не доступна для записи: path=%s", tempFolder));
        }
    }
}
