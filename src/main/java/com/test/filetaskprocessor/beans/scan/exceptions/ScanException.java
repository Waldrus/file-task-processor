package com.test.filetaskprocessor.beans.scan.exceptions;

public class ScanException extends Exception {
    public ScanException(String message) {
        super(message);
    }

    public ScanException(String message, Throwable cause) {
        super(message, cause);
    }
}
