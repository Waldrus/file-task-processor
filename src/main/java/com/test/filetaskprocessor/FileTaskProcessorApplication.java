package com.test.filetaskprocessor;

import com.test.filetaskprocessor.beans.config.TaskConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FileTaskProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileTaskProcessorApplication.class, args);
    }


}
